package com.dolgiy.messageTransmition.coders;

public class MessageHider  {


    public byte[] code(byte[] message, byte[] mask, byte[] pseudoRandomSequence){
        byte[] result = new byte[pseudoRandomSequence.length];
        for(int i = 0; i < message.length; i++){
            result[i] = (byte) (message[i] ^ mask[i] ^ pseudoRandomSequence[i]);
        }
        return result;
    }

    public byte[] decode(byte[] codedMessage, byte[] mask, byte[] pseudoRandomSequence) {
        for(int i = 0; i < mask.length; i++){
            mask[i] = (byte) (codedMessage[i] ^ pseudoRandomSequence[i] ^ mask[i]);
        }
        return mask;
    }

}
