package com.dolgiy.messageTransmition.coders;

/**
 * For classes that provides some operations with bytes.
 */
public interface ByteCoder {
    byte[] code(byte[] bytes);
}
