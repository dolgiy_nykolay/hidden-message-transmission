package com.dolgiy.messageTransmition.generators;

import java.util.Random;

/**
 *
 */
public class RandomSequenceGenerator implements ByteGenerator{

    private int bytesCount;
    private Random random;

    /**
     * Constructs object with given parameters.
     * @param bytesCount Gets the number of byte that {@link #generate()} method returns.
     */
    public RandomSequenceGenerator(int bytesCount) {
        this.bytesCount = bytesCount;
        this.random = new Random();
    }

    /**
     * Constructs object with given parameters.
     * @param bytesCount Gets the number of byte that {@link #generate()} method returns.
     * @param random Gets custom Random object that {@link #generate()} method will use in generation.
     */
    public RandomSequenceGenerator(int bytesCount, Random random) {
        this.bytesCount = bytesCount;
        this.random = random;
    }

    /**
     * Generates random sequence of bytes.
     * @return Random sequence of bytes.
     */
    @Override
    public byte[] generate() {
        byte[] result = new byte[bytesCount];
        random.nextBytes(result);
        return result;
    }
}
