package com.dolgiy.messageTransmition.generators;

/**
 * For classes that can generate {@link byte[]}
 */
public interface ByteGenerator {
    byte[] generate();
}
