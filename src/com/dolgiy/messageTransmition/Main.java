package com.dolgiy.messageTransmition;

import com.dolgiy.messageTransmition.coders.ByteCoder;
import com.dolgiy.messageTransmition.coders.MessageHider;
import com.dolgiy.messageTransmition.generators.ByteGenerator;
import com.dolgiy.messageTransmition.generators.RandomSequenceGenerator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.SQLOutput;
import java.util.Arrays;

public class Main {

    private static final String CHARSET = "UTF-8";

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        MessageHider hider = new MessageHider();
        byte[] message;
        byte[] codedMessage;
        byte[] decodedMessage;
        byte[] bigRandom;
        byte[] smallRandom;
        ByteGenerator generator;
        System.out.print("Enter string for coding or /end for finish.\n> ");
        String input = in.readLine();
        while(!input.equals("/end")){
            message = input.getBytes(Charset.forName(CHARSET));
            generator = new RandomSequenceGenerator((int) (message.length * 4 * 1.5));
            bigRandom = generator.generate();
            generator = new RandomSequenceGenerator(message.length * 2);
            smallRandom = generator.generate();

            codedMessage = hider.code(message,smallRandom,bigRandom);
            decodedMessage = hider.decode(codedMessage,smallRandom,bigRandom);

            System.out.println("Message         : " + input);
            System.out.println("Coded message   : " + new String(codedMessage,Charset.forName(CHARSET)));
            System.out.println("Decoded message : " + new String(decodedMessage, Charset.forName(CHARSET)));
            System.out.println(" - - - - - - - - - - - - - - - - - - - - - - - - - - -");
            System.out.print("Enter string for coding or /end for finish.\n> ");
            input = in.readLine();
        }





    }
}
